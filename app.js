const express = require('express');
const PORT = 3000;
const cors = require('cors');
const multer = require('multer');
const passport = require('passport');
const mongoose = require('mongoose');
const config = require('./config');
const app = express();
// config express
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// on utilise le package passport
app.use(passport.initialize());
const passportMiddleware = require('./src/middleware/passport');
passport.use(passportMiddleware);

// la route classique et les routes spécifiques
app.get('/', (req,res)=>{
    res.send('The server is running on port '+PORT);
});
const routes = require('./src/routes/userRoutes');
app.use('/', routes);
app.listen(PORT);
// on crée un schéma pour les images
const ImageSchema = new mongoose.Schema({
    filename: {
        type: String,
        required: 'the filename is mandatory !'
    },
    title: String,
    description: String,
    author: String,
    likes: Number,
    upload_date: {
        type: Date,
        default: Date.now
    }
});
// on définit un model et une méthode pour ajouter une image
// dans la BDD MongoDB GALLERY_DB
const ImageModel = mongoose.model('Image',ImageSchema);
const addNewImage = (req,res) => {
    const imageData = {
        filename: req.file.filename,
        title: req.body.title,
        description: req.body.description,
        author: req.body.author
    };
    const newImage = new ImageModel( imageData );
    newImage.save( (error,data) => {
        if (error) res.send(error);
        res.json(data);
        console.log('Method addNewImage :', data);
    });
}
// la méthode pour afficher le listing de photos
const getImages = (req,res) => {
    ImageModel.find({},(err,data) => {
        if (err) res.send(err);
        res.json(data);
        console.log('Method getImages :', data);
    }).sort({'upload_date':-1});
}
// on définit quelques constantes
const UPLOAD_DIR = 'public';
const message = 'Server is running on port '+PORT;
// on gère l'upload dans le dossier voulu avec un renommage
app.use(express.static(UPLOAD_DIR));
const storage = multer.diskStorage({
    destination: (req,file,cb) => {
        cb(null,UPLOAD_DIR)
    },
    filename: (req,file,cb) => {
        const ext = file.mimetype=='image/jpeg' ? '.jpg' : '.png';
        cb(null, file.fieldname + '-' + Date.now() + ext)
    }
});
const upload = multer({ storage: storage });
// on définit notre route pour l'upload en méthode post
app.post('/', upload.single('picture'), addNewImage );
// la route pour afficher la liste au format Json
app.get('/list', getImages );
// la route pour l'accès par défaut
// la route pour l'accès par défaut
app.get('/', (req,res) => {
    res.send(message);
    console.log( req.headers.host );
});

// on se connecte au serveur MongoDB
mongoose.connect(config.db, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true }, () => {
    console.log('MongoDB connected successfully !!!');
});
